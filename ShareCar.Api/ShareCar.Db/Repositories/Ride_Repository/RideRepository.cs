﻿using ShareCar.Db;
using ShareCar.Db.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using ShareCar.Db.Repositories;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace ShareCar.Db.Repositories.Ride_Repository
{
    public class RideRepository : IRideRepository
    {

        private readonly ApplicationDbContext _databaseContext;
        
        public RideRepository(ApplicationDbContext context)
        {
            _databaseContext = context;
        }

        public void AddRide(Ride ride)
        {
            ride.isActive = true;
            _databaseContext.Rides.Add(ride);
            _databaseContext.SaveChanges();
        }

        public IEnumerable<Ride> GetRidesByDate(DateTime date)
        {
            return _databaseContext.Rides
                    .Where(x => x.RideDateTime == date && x.isActive);
        }

        public IEnumerable<Ride> GetRidesByDestination(int addressToId)
        {
            return _databaseContext.Rides
                .Where(x => x.Route.ToId == addressToId && x.isActive);
        }

        public Ride GetRideById(int id)
        {
            try
            {
                return _databaseContext.Rides.Single(x => x.RideId == id && x.isActive); // Throws exception if ride is not found
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Ride> GetRidesByStartPoint(int addressFromId)
        {
            return _databaseContext.Rides
                .Where(x => x.Route.FromId == addressFromId && x.isActive);
        }

        public IEnumerable<Passenger> GetPassengersByRideId(int id)
        {
            return _databaseContext.Passengers.Where(x => x.RideId == id);
        }
        
        public IEnumerable<Ride> GetRidesByPassenger(Passenger passenger)
        {
            return _databaseContext.Rides.Where(x => x.Passengers.Contains(passenger) && x.isActive);
        }

        public void UpdateRide(Ride ride)
        {

                Ride rideToUpdate = _databaseContext.Rides.Where(x => x.RideId == ride.RideId).Single();
                rideToUpdate.RouteId = ride.RouteId;
                rideToUpdate.RideDateTime = ride.RideDateTime;
                rideToUpdate.isActive = true;
                rideToUpdate.NumberOfSeats = ride.NumberOfSeats;
                
                _databaseContext.Rides.Update(rideToUpdate);
                _databaseContext.SaveChanges();

        }

        public void SetRideAsInactive(Ride ride)
        {
            Ride rideToDelete = _databaseContext.Rides.Include(x => x.Requests).SingleOrDefault(x => x.RideId == ride.RideId);
            rideToDelete.isActive = false;
            _databaseContext.SaveChanges();

        }

        public IEnumerable<Ride> GetRidesByDriver(string email)
        {
            return _databaseContext.Rides
                .Include(x=>x.Requests)
                .Include(x=>x.Passengers)
                .Where(x => x.DriverEmail == email && x.isActive == true);
        }

        public IEnumerable<Ride> GetRidesByRoute(string routeGeometry)
        {
            return _databaseContext.Rides.Where(x => x.Route.Geometry == routeGeometry && x.isActive);
        }
    }
}
    
